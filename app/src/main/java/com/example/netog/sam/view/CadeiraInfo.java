package com.example.netog.sam.view;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.example.netog.sam.R;
import com.example.netog.sam.controller.CadeiraFachada;
import com.example.netog.sam.controller.EnqueteFachada;
import com.example.netog.sam.controller.NotificacaoFachada;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

public class CadeiraInfo extends BaseFragment{

    CadeiraFachada fachada = CadeiraFachada.getInstance();
    NotificacaoFachada fachadaNotificacao = NotificacaoFachada.getInstance();
    EnqueteFachada fachadaEnquete = EnqueteFachada.getInstance();

    @SuppressLint({"NewApi", "SetTextI18n", "CommitTransaction"})
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        @SuppressLint("SimpleDateFormat") SimpleDateFormat formataData = new SimpleDateFormat("dd-MM-yyyy");
        View view = inflater.inflate(R.layout.info_cadeira, container, false);
        TextView data = view.findViewById(R.id.txt_data);
        Button dia1 = view.findViewById(R.id.btn_dia_ter);
        dia1.setBackground(getResources().getDrawable(R.drawable.ic_dia_enable));
        dia1.setOnClickListener(fachada.abrirInfoMonitoria(Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction()));
        Button dia2 = view.findViewById(R.id.btn_dia_qui);
        dia2.setBackground(getResources().getDrawable(R.drawable.ic_dia_enable));
        dia2.setOnClickListener(fachada.abrirInfoMonitoria(Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction()));
        data.setText(formataData.format(new Date()).replace("-","/"));
        Button btnNoticias = view.findViewById(R.id.btn_noticias);
        Button btnEnquete = view .findViewById(R.id.btn_enquetes);
        btnNoticias.setOnClickListener(fachadaNotificacao.abrirInfo(Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction()));
        btnEnquete.setOnClickListener(fachadaEnquete.abrirInfo(Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction()));
        return view;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onBackPressed() {
        @SuppressLint("CommitTransaction") FragmentTransaction ft = Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.painel_principal, new MinhasCadeiras());
        ft.commit();
    }
}
