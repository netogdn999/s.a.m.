/**
 * @author: neto
 * @e-mail: netogdn999@gmail.com
 * @license:
 *
 **/
package com.example.netog.sam.model;

import com.example.netog.sam.controller.ExceptionController;
import org.json.JSONException;
import org.json.JSONObject;

public class Cadeira {
    private int _id;
    private String nome;
    private String curso;
    private int semestre;

    public Cadeira() {
        super();
    }

    public Cadeira(String nome, String curso, int semestre) {
        this();
        this.nome = nome;
        this.curso = curso;
        this.semestre = semestre;
    }

    public Cadeira(int _id, String nome, String curso, int semestre) {
        this(nome, curso, semestre);
        this._id = _id;
    }

    public int getId() {
        return _id;
    }

    public void setId(int _id) {
        this._id = _id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCurso() {
        return curso;
    }

    public void setCurso(String curso) {
        this.curso = curso;
    }

    public int getSemestre() {
        return semestre;
    }

    public void setSemestre(int semestre) {
        this.semestre = semestre;
    }

    @Override
    public String toString(){
        JSONObject json = new JSONObject();
        try {
            json.put("id",getId());
            json.put("nome",getNome());
            json.put("curso",getCurso());
            json.put("semestre",getSemestre());
        } catch (JSONException e) {
            ExceptionController.setMensagem(new String[]{"Error","Error Cadeira model "+e.getMessage()});
        }
        return json.toString();
    }

    @Override
    public boolean equals(Object obj) {
        return obj.toString().equals(toString());
    }
}
