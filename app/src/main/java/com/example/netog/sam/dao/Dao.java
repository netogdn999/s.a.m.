/**
 * @author: neto
 * @e-mail: netogdn999@gmail.com
 * @license:
 *
 **/
package com.example.netog.sam.dao;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

public abstract class Dao<T> {
    protected static BDCore bdCore;
    protected static HttpsURLConnection http;

    protected Dao(BDCore bd) {
        if(bdCore == null){
            bdCore = bd;
        }
    }

    protected void http(String url) throws IOException {
        if(http == null){
            http = BDCore.getHttp(url);
        }
    }

    public abstract void cadastrar(T objeto) throws Exception;
    public abstract void remover(T objeto) throws Exception;
    public abstract void atualizar(T objeto) throws Exception;
    public abstract List<T> listar();
    public abstract List<T> listar(String coluna, String valor);
}
