/**
 * @author: neto
 * @e-mail: netogdn999@gmail.com
 * @license:
 *
 **/
package com.example.netog.sam.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.JsonReader;
import android.util.Log;

import com.example.netog.sam.model.Agenda;
import com.example.netog.sam.model.Cadeira;
import com.example.netog.sam.model.Usuario;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Objects;

public class NotificacaoDao extends Dao<Usuario> {
    private SQLiteDatabase bdw;
    private SQLiteDatabase bdr;

    public NotificacaoDao(Context context){
        super(BDCore.getInstance(context));
    }

    @Override
    public void cadastrar(Usuario objeto) throws Exception {
        //toDo:refatorar nome do parametro
        try {
            if (http.getResponseCode() == 200) {
                InputStream responseBody = http.getInputStream();
                InputStreamReader rbr = new InputStreamReader(responseBody, "UTF-8");
                JsonReader jr = new JsonReader(rbr);
                String text="";
                jr.beginObject();
                while(jr.hasNext()){
                    text+=jr.nextName()+" : ";
                    text+=jr.nextString()+"\n";
                }
                Log.i("Http", text);
                jr.close();
            } else {
                Log.i("Http","Erro "+http.getResponseCode()+" "+http.getResponseMessage()+"\n");
            }
        }catch (Exception e){
            throw new Exception("Erro no cadastrar-Usuario "+e.getMessage());
        }finally {
            http.disconnect();
        }
    }

    @Override
    public void remover(Usuario objeto) throws Exception {
        try {

        }catch (Exception e){
            throw new Exception("Erro no remover-Usuario"+e.getMessage());
        }finally {
            bdw.close();
        }
    }

    @Override
    public void atualizar(Usuario objeto) throws Exception {
        try {

        }catch (Exception e){
            throw new Exception("Erro no atualizar-Usuario"+e.getMessage());
        }finally {
            bdw.close();
        }
    }

    @Override
    public List<Usuario> listar() {
        try{

        }catch (Exception e){
            throw new Exception("Erro no listar-Usuario"+e.getMessage());
        }finally {
            bdr.close();
            return null;
        }
    }

    @Override
    public List<Usuario> listar(String coluna, String valor) {
        try{

        }catch (Exception e){
            throw new Exception("Erro no listarFiltro-Usuario"+e.getMessage());
        }finally {
            bdr.close();
            return null;
        }
    }

    public List<Agenda> listarAgenda() {
        try{

        }catch (Exception e){
            throw new Exception("Erro no listarAgenda-Usuario"+e.getMessage());
        }finally {
            bdr.close();
            return null;
        }
    }

    public List<Cadeira> listarCadeira() {
        try{

        }catch (Exception e){
            throw new Exception("Erro no listarCadeira-Usuario"+e.getMessage());
        }finally {
            bdr.close();
            return null;
        }
    }

    public void setHttp(String url) throws IOException {
        http(url);
    }
}
