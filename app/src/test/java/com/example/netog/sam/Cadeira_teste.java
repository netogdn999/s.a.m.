package com.example.netog.sam;

import com.example.netog.sam.model.Cadeira;
import org.junit.Test;
import org.mockito.Mock;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class Cadeira_teste {

    @Test
    public void criarObjeto(){
        Cadeira c = new Cadeira(0,"Matematica", "Engenharia de Software", 6);
        assertEquals("Nome inválido","Matematica", c.getNome());
        assertEquals("Curso inválido","Engenharia de Software", c.getCurso());
        assertEquals("Semestre inválido",6, c.getSemestre());
        assertSame("parametros do construtor da classe cadeira estão incorretos", Cadeira.class, c.getClass());
    }
}
