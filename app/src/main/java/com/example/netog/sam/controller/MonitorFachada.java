/**
 * @author: neto
 * @e-mail: netogdn999@gmail.com
 * @license:
 *
 **/
package com.example.netog.sam.controller;

import android.annotation.SuppressLint;
import android.content.Context;
import com.example.netog.sam.model.Monitor;
import java.util.List;

public class MonitorFachada extends Fachada {
    @SuppressLint("StaticFieldLeak")private static MonitorFachada fachada;

    private MonitorFachada(Context context) {
        super(context);
        getDao("monitor", context);
    }

    public static MonitorFachada getInstance(){
        if(MonitorFachada.fachada == null){
            MonitorFachada.fachada = new MonitorFachada(context);
        }
        return MonitorFachada.fachada;
    }

    public void cadastrar() throws Exception {
        dao.cadastrar(new Monitor());
    }
    public void remover() throws Exception {
        dao.remover(new Monitor());
    }
    public List<Monitor> listar(){
        return dao.listar();
    }
    public List<Monitor> listar(String coluna, String valor){
        return dao.listar(coluna, valor);
    }
}
