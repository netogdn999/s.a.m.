/**
 * @author: neto
 * @e-mail: netogdn999@gmail.com
 * @license:
 *
 **/
package com.example.netog.sam.view.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.netog.sam.R;
import com.example.netog.sam.controller.EnqueteFachada;
import com.example.netog.sam.model.Enquete;

import java.util.List;

public class EnqueteAdapter extends RecyclerView.Adapter<EnqueteAdapter.ViewHolder> {
    private final EnqueteFachada fachada = EnqueteFachada.getInstance();
    private final Context mContext;
    private final List<Enquete> notificacaoList;

    public EnqueteAdapter(Context mContext, List<Enquete> notificacaoList) {
        this.mContext = mContext;
        this.notificacaoList = notificacaoList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.enquete_item, parent, false);
        return new ViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        Enquete notificacao = notificacaoList.get(position);
        holder.pergunta.setText(notificacao.getPerguntas().get(0).getDescricao());
        holder.overflow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });
    }

    @Override
    public int getItemCount() {
        return notificacaoList.size();
    }

    /*popup*/
    private void showPopupMenu(View view) {
        PopupMenu popup = new PopupMenu(mContext, view);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.opcoes_card_item, popup.getMenu());
        popup.setOnMenuItemClickListener(new MyMenuItemClickListener());
        popup.show();
    }

    /*classes*/
    class MyMenuItemClickListener implements PopupMenu.OnMenuItemClickListener {

        MyMenuItemClickListener() {
            super();
        }

        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {
            switch (menuItem.getItemId()) {
                case R.id.action_add_favourite:
                    Toast.makeText(mContext, "Add to favourite", Toast.LENGTH_SHORT).show();
                    return true;
                case R.id.action_play_next:
                    Toast.makeText(mContext, "Play next", Toast.LENGTH_SHORT).show();
                    return true;
                default:
            }
            return false;
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        final TextView pergunta;
        final ImageView overflow;

        ViewHolder(View view) {
            super(view);
            pergunta = view.findViewById(R.id.pergunta);
            overflow = view.findViewById(R.id.overflow);
        }
    }
}
