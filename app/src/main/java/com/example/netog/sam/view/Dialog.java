package com.example.netog.sam.view;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.netog.sam.R;
import com.example.netog.sam.controller.CadeiraFachada;
import com.example.netog.sam.controller.ExceptionController;
import com.example.netog.sam.controller.Fachada;
import com.example.netog.sam.controller.NotificacaoFachada;

public abstract class Dialog {

    private static AlertDialog dialog;
    @SuppressLint("StaticFieldLeak")
    private static Fachada fachada;

    public static void showMessageDialog(View view, LayoutInflater layout, String titulo, String mensagem, final int tempo, Fachada fachada){
        Dialog.fachada = fachada;
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(view.getContext());
        @SuppressLint("InflateParams") final View alert = layout.inflate(R.layout.alert_dialog_message, null);
        TextView title = alert.findViewById(R.id.titulo);
        TextView message = alert.findViewById(R.id.mensagem);
        title.setText(titulo);
        message.setText(mensagem);
        mBuilder.setView(alert);
        dialog = mBuilder.create();
        dialog.show();
        dialog.setOnDismissListener(dimiss());
        new Thread(){
            @Override
            public void run() {
                try {
                    sleep(tempo);
                    if(dialog.isShowing()){
                        dialog.cancel();
                    }
                } catch (InterruptedException e) {
                    ExceptionController.setMensagem(new String[]{"Error", "Error no toggle message dialog"});
                    e.printStackTrace();
                }
            }
        }.start();
    }

    public static void showConfirmDialogCallBack(View view, LayoutInflater layout, String tituloConfirm, String mensagemConfirm, int position, Fachada fachada){
        Dialog.fachada = fachada;
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(view.getContext());
        @SuppressLint("InflateParams") View alert = layout.inflate(R.layout.alert_dialog_confirm, null);
        Button btn_sim = alert.findViewById(R.id.btn_sim);
        Button btn_nao = alert.findViewById(R.id.btn_nao);
        TextView title = alert.findViewById(R.id.titulo);
        TextView message = alert.findViewById(R.id.mensagem);
        TextView close = alert.findViewById(R.id.btn_close);
        title.setText(tituloConfirm);
        message.setText(mensagemConfirm);
        close.setOnClickListener(cancelar());
        btn_sim.setOnClickListener(excluir(position));
        btn_nao.setOnClickListener(cancelar());
        mBuilder.setView(alert);
        dialog = mBuilder.create();
        dialog.show();
        dialog.setOnDismissListener(dimiss());
    }

    private static View.OnClickListener excluir(final int position){
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if(fachada instanceof CadeiraFachada) {
                        ((CadeiraFachada) fachada).remover(position);
                        dialog.cancel();
                    }else if(fachada instanceof NotificacaoFachada){
                        ((NotificacaoFachada) fachada).remover();
                        dialog.cancel();
                    }
                } catch (Exception e) {
                    ExceptionController.setMensagem(new String[]{"Error","Error ao excluir "+e.getMessage()});
                    e.getStackTrace();
                }
            }
        };
    }

    private static View.OnClickListener cancelar(){
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               dialog.cancel();
            }
        };
    }

    private static AlertDialog.OnDismissListener dimiss(){
        return new AlertDialog.OnDismissListener(){
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                dialog.cancel();
            }
        };
    }
}
