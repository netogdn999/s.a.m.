/**
 * @author: neto
 * @e-mail: netogdn999@gmail.com
 * @license:
 *
 **/
package com.example.netog.sam.controller;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;

import com.example.netog.sam.R;
import com.example.netog.sam.model.Enquete;
import com.example.netog.sam.view.CadeiraInfo;
import com.example.netog.sam.view.Info;
import com.example.netog.sam.view.InfoMonitoria;
import com.example.netog.sam.view.adapters.CadeirasAdapter;

import java.util.List;

public class CadeiraFachada extends Fachada {
    @SuppressLint("StaticFieldLeak")private static CadeiraFachada fachada;
    private CadeirasAdapter cadeirasAdapter;

    private CadeiraFachada(Context context) {
        super(context);
        getDao("enquete", context);
    }

    public static CadeiraFachada getInstance(){
        if(CadeiraFachada.fachada == null){
            CadeiraFachada.fachada = new CadeiraFachada(context);
        }
        return CadeiraFachada.fachada;
    }

    public View.OnClickListener abrirInfo(final FragmentTransaction ft){
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fra = new CadeiraInfo();
                ft.replace(R.id.painel_principal, fra);
                ft.commit();
            }
        };
    }

    public View.OnClickListener abrirInfoMonitoria(final FragmentTransaction ft){
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fra = new InfoMonitoria();
                ft.replace(R.id.painel_principal, fra);
                ft.commit();
            }
        };
    }

    public void cadastrar() throws Exception {

    }
    public void remover(int position) throws Exception {
        Log.i("remover", "posição: "+position+" item: "+cadeirasAdapter.getCadeiraList().get(position).getNome());
        cadeirasAdapter.getCadeiraList().remove(position);
        cadeirasAdapter.notifyDataSetChanged();
    }
    public List<Enquete> listar(){
        return null;
    }
    public List<Enquete> listar(String coluna, String valor){
        return null;
    }

    public void setCadeirasAdapter(CadeirasAdapter cadeirasAdapter) {
        this.cadeirasAdapter = cadeirasAdapter;
    }
}
