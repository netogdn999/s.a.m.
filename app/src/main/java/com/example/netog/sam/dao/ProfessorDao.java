/**
 * @author: neto
 * @e-mail: netogdn999@gmail.com
 * @license:
 *
 **/
package com.example.netog.sam.dao;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.example.netog.sam.model.Professor;
import java.util.ArrayList;
import java.util.List;

public class ProfessorDao extends Dao<Professor> {
    private SQLiteDatabase bdw;
    private SQLiteDatabase bdr;

    public ProfessorDao(Context context) {
        super(BDCore.getInstance(context));
    }

    @Override
    public void cadastrar(Professor objeto) throws Exception {
        try {
            bdw = bdCore.getWritableDatabase();
            ContentValues valores = new ContentValues();
            valores.put("idUsuario", objeto.getId());
            bdw.insert("professor", null, valores);
        }catch (Exception e){
            throw new Exception("Erro no cadastrar-professor"+e.getMessage());
        }finally {
            bdw.close();
        }
    }

    @Override
    public void remover(Professor objeto) throws Exception {
        try {
            bdw = bdCore.getWritableDatabase();
            bdw.delete("professor", "_id = ?", new String[]{"" + objeto.getIdProfessor()});
        }catch (Exception e){
            throw new Exception("Erro no remover-professor"+e.getMessage());
        }finally {
            bdw.close();
        }
    }

    @Override
    public void atualizar(Professor objeto) throws Exception {
        try {
            bdw = bdCore.getWritableDatabase();
            ContentValues valores = new ContentValues();
            valores.put("idUsuario", objeto.getId());
            bdw.update("professor", valores, "_id = ?", new String[]{""+objeto.getIdProfessor()});
        }catch (Exception e){
            throw new Exception("Erro no atualizar-professor"+e.getMessage());
        }finally {
            bdw.close();
        }
    }

    @Override
    public List<Professor> listar() {
        List<Professor> lista = new ArrayList<>();
        try{
            bdr = bdCore.getReadableDatabase();
            @SuppressLint("Recycle") Cursor cursor = bdr.rawQuery("select professor._id,usuario._id as idUsuario, nome, login, senha from professor join usuario on professor.idUsuario = usuario._id;", null);
            if(cursor.getCount() > 0){
                cursor.moveToFirst();
                do{
                    lista.add(new Professor(cursor.getInt(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getInt(0)));
                }while (cursor.moveToNext());
            }
        }catch (Exception e){
            throw new Exception("Erro no listar-professor"+e.getMessage());
        }finally {
            bdr.close();
            return lista;
        }
    }

    @Override
    public List<Professor> listar(String coluna, String valor) {
        List<Professor> lista = new ArrayList<>();
        try{
            bdr = bdCore.getReadableDatabase();
            String[] filtro = new String[]{coluna, valor};
            @SuppressLint("Recycle") Cursor cursor = bdr.rawQuery("select professor._id,usuario._id as idUsuario, nome, login, senha from professor join usuario on professor.idUsuario = usuario._id where ? like '%?%';", filtro);
            if(cursor.getCount() > 0){
                cursor.moveToFirst();
                do{
                    lista.add(new Professor(cursor.getInt(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getInt(0)));
                }while (cursor.moveToNext());
            }
        }catch (Exception e){
            throw new Exception("Erro no listarFiltro-professor"+e.getMessage());
        }finally {
            bdr.close();
            return lista;
        }
    }
}
