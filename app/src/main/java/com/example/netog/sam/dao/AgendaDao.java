/**
 * @author: neto
 * @e-mail: netogdn999@gmail.com
 * @license:
 *
 **/
package com.example.netog.sam.dao;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.example.netog.sam.model.Agenda;
import java.util.ArrayList;
import java.util.List;

public class AgendaDao extends Dao<Agenda> {
    private SQLiteDatabase bdw;
    private SQLiteDatabase bdr;

    public AgendaDao(Context context) {
        super(BDCore.getInstance(context));
    }

    @Override
    public void cadastrar(Agenda objeto) throws Exception {
        try {
            bdw = bdCore.getWritableDatabase();
            ContentValues valores = new ContentValues();
            valores.put("status", objeto.getStatus());
            valores.put("descricao", objeto.getDescricao());
            valores.put("dia", objeto.getDia());
            valores.put("horario", objeto.getHorario());
            bdw.insert("agenda", null, valores);
        }catch (Exception e){
            throw new Exception("Erro no cadastrar-agenda"+e.getMessage());
        }finally {
            bdw.close();
        }
    }

    @Override
    public void remover(Agenda objeto) throws Exception {
        try {
            bdw = bdCore.getWritableDatabase();
            bdw.delete("agenda", "_id = ?", new String[]{"" + objeto.get_id()});
        }catch (Exception e){
            throw new Exception("Erro no remover-agenda"+e.getMessage());
        }finally {
            bdw.close();
        }
    }

    @Override
    public void atualizar(Agenda objeto) throws Exception {
        try {
            bdw = bdCore.getWritableDatabase();
            ContentValues valores = new ContentValues();
            valores.put("status", objeto.getStatus());
            valores.put("descricao", objeto.getDescricao());
            valores.put("dia", objeto.getDia());
            valores.put("horario", objeto.getHorario());
            bdw.update("agenda", valores, "_id = ?", new String[]{""+objeto.get_id()});
        }catch (Exception e){
            throw new Exception("Erro no atualizar-agenda"+e.getMessage());
        }finally {
            bdw.close();
        }
    }

    @Override
    public List<Agenda> listar() {
        List<Agenda> lista = new ArrayList<>();
        try{
            bdr = bdCore.getReadableDatabase();
            String[] colunas = new String[]{"_id", "status", "descricao", "dia", "horario"};
            @SuppressLint("Recycle") Cursor cursor = bdr.query("agenda", colunas, null, null, null, null, "nome ASC");
            if(cursor.getCount() > 0){
                cursor.moveToFirst();
                do{
                    lista.add(new Agenda(cursor.getInt(0), cursor.getInt(1), cursor.getString(2), cursor.getString(3), cursor.getString(4)));
                }while (cursor.moveToNext());
            }
        }catch (Exception e){
            throw new Exception("Erro no listar-agenda"+e.getMessage());
        }finally {
            bdr.close();
            return lista;
        }
    }

    @Override
    public List<Agenda> listar(String coluna, String valor) {
        List<Agenda> lista = new ArrayList<>();
        try{
            bdr = bdCore.getReadableDatabase();
            String[] filtro = new String[]{coluna, valor};
            String[] colunas = new String[]{"_id", "status", "descricao", "dia", "horario"};
            @SuppressLint("Recycle") Cursor cursor = bdr.query("agenda", colunas, " ? like '%?%' ", filtro, null, null, "nome ASC");
            if(cursor.getCount() > 0){
                cursor.moveToFirst();
                do{
                    lista.add(new Agenda(cursor.getInt(0), cursor.getInt(1), cursor.getString(2), cursor.getString(3), cursor.getString(4)));
                }while (cursor.moveToNext());
            }
        }catch (Exception e){
            throw new Exception("Erro no listarFiltro-agenda"+e.getMessage());
        }finally {
            bdr.close();
            return lista;
        }
    }
}
