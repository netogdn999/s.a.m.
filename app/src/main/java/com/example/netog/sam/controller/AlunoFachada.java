/**
 * @author: neto
 * @e-mail: netogdn999@gmail.com
 * @license:
 *
 **/
package com.example.netog.sam.controller;

import android.annotation.SuppressLint;
import android.content.Context;
import com.example.netog.sam.model.Aluno;
import java.util.List;

public class AlunoFachada extends Fachada {
    @SuppressLint("StaticFieldLeak")private static AlunoFachada fachada;

    private AlunoFachada(Context context) {
        super(context);
        getDao("aluno", context);
    }

    public static AlunoFachada getInstance(){
        if(AlunoFachada.fachada == null){
            AlunoFachada.fachada = new AlunoFachada(context);
        }
        return AlunoFachada.fachada;
    }

    public void cadastrar() throws Exception {
        dao.cadastrar(new Aluno());
    }
    public void remover() throws Exception {
        dao.remover(new Aluno());
    }
    public void alterar() throws Exception {dao.atualizar(new Aluno());}
    public List<Aluno> listar(){
        return dao.listar();
    }
    public List<Aluno> listar(String coluna, String valor){
        return dao.listar(coluna, valor);
    }
}
