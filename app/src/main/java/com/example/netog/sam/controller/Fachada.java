/**
 * @author: neto
 * @e-mail: netogdn999@gmail.com
 * @license:
 *
 **/
package com.example.netog.sam.controller;

import android.annotation.SuppressLint;
import android.content.Context;
import com.example.netog.sam.dao.AlunoDao;
import com.example.netog.sam.dao.Dao;
import com.example.netog.sam.dao.EnqueteDao;
import com.example.netog.sam.dao.MonitorDao;
import com.example.netog.sam.dao.NotificacaoDao;
import com.example.netog.sam.dao.PerguntaDao;
import com.example.netog.sam.dao.ProfessorDao;

import java.io.IOException;
import java.net.HttpURLConnection;

public class Fachada {
    @SuppressLint("StaticFieldLeak")private static Fachada fachadaInstancia;
    @SuppressLint("StaticFieldLeak")static Context context;
    Dao dao;
    HttpURLConnection http;

    Fachada(Context context){
        super();
        Fachada.context = context;
    }
    //toDO: JWT
    public static void instancia(Context context){
        if(Fachada.fachadaInstancia == null){
            Fachada.fachadaInstancia = new Fachada(context);
        }
    }

    void getDao(String banco, Context context){
        switch (banco){
            case "aluno":
                this.dao = new AlunoDao(context);
            break;
            case "enquete":
                this.dao = new EnqueteDao(context);
            break;
            case "monitor":
                this.dao = new MonitorDao(context);
            break;
            case "pergunta":
                this.dao = new PerguntaDao(context);
            break;
            case "professor":
                this.dao = new ProfessorDao(context);
            break;
            case "notificacao":
                this.dao = new NotificacaoDao(context);
            break;
        }
    }
    void setDao(Dao dao) {
        this.dao = dao;
    }

    void http(String url) throws IOException {
        ((NotificacaoDao) dao).setHttp(url);
    }

    public Context getContext(){
        return Fachada.context;
    }
}
