/**
 * @author: neto
 * @e-mail: netogdn999@gmail.com
 * @license:
 *
 **/
package com.example.netog.sam.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class BDCore extends SQLiteOpenHelper{
    private static final String NAME_BD = "sam";
    private static final int VERSION_BD = 1;
    private static BDCore bdCore;
    private static HttpsURLConnection http;

    private BDCore(Context context) {
        super(context, NAME_BD, null, VERSION_BD);
    }

    static BDCore getInstance(Context context){
        if(BDCore.bdCore == null){
            BDCore.bdCore = new BDCore(context);
        }
        return BDCore.bdCore;
    }

    static HttpsURLConnection getHttp(final String url) throws IOException {
        if(BDCore.http == null){
            http = (HttpsURLConnection) (new URL(url)).openConnection();
            http.setRequestProperty("User-Agent", "S.A.M-v1.0.0");
        }
        return http;
    }

    @Override
    public void onCreate(SQLiteDatabase bd) {
        criaUsuario(bd);
        criaAluno(bd);
        criaMonitor(bd);
        criaProfessor(bd);
        criaAgenda(bd);
        criaUsuarioAgenda(bd);
        criaCadeira(bd);
        criaUsuarioCadeira(bd);
        criaEnquete(bd);
        criaPergunta(bd);
        criaPerguntaEnquete(bd);
    }

    @Override
    public void onUpgrade(SQLiteDatabase bd, int i, int i1) {
        bd.execSQL("drop table usuario;");
        onCreate(bd);
    }

    private void criaUsuario(SQLiteDatabase bd){
        bd.execSQL("create table usuario(" +
                "_id integer primary key autoincrement," +
                "nome text not null," +
                "login text not null," +
                "senha text not null);");
    }
    private void criaAluno(SQLiteDatabase bd){
        bd.execSQL("create table aluno(" +
                "_id integer primary key autoincrement," +
                "idUsuario integer not null," +
                "curso text not null," +
                "semestre integer not null," +
                "foreign key (idUsuario) references usuario(_id));");
    }
    private void criaMonitor(SQLiteDatabase bd){
        bd.execSQL("create table monitor(" +
                "_id integer primary key autoincrement," +
                "idUsuario integer not null," +
                "foreign key (idUsuario) references usuario(_id));");
    }
    private void criaProfessor(SQLiteDatabase bd){
        bd.execSQL("create table professor(" +
                "_id integer primary key autoincrement," +
                "idUsuario integer not null," +
                "foreign key (idUsuario) references usuario(_id));");
    }
    private void criaAgenda(SQLiteDatabase bd){
        bd.execSQL("create table agenda(" +
                "_id integer primary key autoincrement," +
                "status integer not null," +
                "descricao text not null," +
                "dia text not null," +
                "horario text not null);");
    }
    private void criaUsuarioAgenda(SQLiteDatabase bd){
        bd.execSQL("create table usuario_agenda(" +
                "idUsuario integer not null," +
                "idAgenda integer not null," +
                "foreign key (idUsuario) references usuario(_id)," +
                "foreign key (idAgenda) references agenda(_id));");
    }
    private void criaCadeira(SQLiteDatabase bd){
        bd.execSQL("create table cadeira(" +
                "_id integer primary key autoincrement," +
                "nome text not null," +
                "curso text not null);");
    }
    private void criaUsuarioCadeira(SQLiteDatabase bd){
        bd.execSQL("create table usuario_cadeira(" +
                "idUsuario integer not null," +
                "idCadeira integer not null," +
                "foreign key (idUsuario) references usuario(_id)," +
                "foreign key (idCadeira) references cadeira(_id));");
    }
    private void criaEnquete(SQLiteDatabase bd){
        bd.execSQL("create table Enquete(" +
                "_id integer primary key autoincrement," +
                "idUsuario integer not null," +
                "dataInicial integer not null," +
                "dataFinal integer not null," +
                "foreign key (idUsuario) references usuario(_id));");
    }
    private void criaPergunta(SQLiteDatabase bd){
        bd.execSQL("create table Pergunta(" +
                "_id integer primary key autoincrement," +
                "idUsuario integer not null," +
                "dataInicial integer not null," +
                "dataFinal integer not null," +
                "foreign key (idUsuario) references usuario(_id));");
    }
    private void criaPerguntaEnquete(SQLiteDatabase bd){
        bd.execSQL("create table pergunta_enquete(" +
                "idPergunta integer not null," +
                "idEnquete integer not null," +
                "foreign key (idPergunta) references pergunta(_id)," +
                "foreign key (idEnquete) references enquete(_id));");
    }
}
