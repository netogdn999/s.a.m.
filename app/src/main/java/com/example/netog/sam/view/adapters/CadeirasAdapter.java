/**
 * @author: neto
 * @e-mail: netogdn999@gmail.com
 * @license:
 *
 **/
package com.example.netog.sam.view.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.example.netog.sam.R;
import com.example.netog.sam.controller.CadeiraFachada;
import com.example.netog.sam.model.Cadeira;
import com.example.netog.sam.view.Dialog;

import java.util.List;

public class CadeirasAdapter extends RecyclerView.Adapter<CadeirasAdapter.ViewHolder> {
    private final CadeiraFachada fachada = CadeiraFachada.getInstance();
    private final Context mContext;
    private final List<Cadeira> cadeiraList;
    private final FragmentTransaction ft;
    private final LayoutInflater layout;

    public CadeirasAdapter(Context mContext, List<Cadeira> cadeiraList, FragmentTransaction ft) {
        this.mContext = mContext;
        this.cadeiraList = cadeiraList;
        this.ft = ft;
        layout = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        fachada.setCadeirasAdapter(this);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.cadeira_item, parent, false);
        return new ViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        Cadeira cadeira = cadeiraList.get(position);
        holder.title.setText(cadeira.getNome());
        holder.count.setText(cadeira.getCurso() + "" + cadeira.getSemestre());
        Glide.with(mContext).load(R.drawable.ic_math).into(holder.thumbnail);
        holder.thumbnail.setOnClickListener(fachada.abrirInfo(ft));
        holder.overflow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPopupMenu(holder.overflow);
            }
        });
        holder.close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Dialog.showConfirmDialogCallBack(holder.view, layout, "Deletar", "Deseja deletar a cadeira "+holder.title.getText(), position, fachada);
            }
        });
    }

    @Override
    public int getItemCount() {
        return cadeiraList.size();
    }

    /*popup*/
    private void showPopupMenu(View view) {
        PopupMenu popup = new PopupMenu(mContext, view);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.opcoes_card_item, popup.getMenu());
        popup.setOnMenuItemClickListener(new MyMenuItemClickListener());
        popup.show();
    }

    /*classes*/
    class MyMenuItemClickListener implements PopupMenu.OnMenuItemClickListener {

        MyMenuItemClickListener() {
            super();
        }

        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {
            switch (menuItem.getItemId()) {
                case R.id.action_add_favourite:
                    Toast.makeText(mContext, "Add to favourite", Toast.LENGTH_SHORT).show();
                    return true;
                case R.id.action_play_next:
                    Toast.makeText(mContext, "Play next", Toast.LENGTH_SHORT).show();
                    return true;
                default:
            }
            return false;
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        final TextView title, count, close;
        final ImageView thumbnail, overflow;
        final View view;

        ViewHolder(View view) {
            super(view);
            this.view = view;
            title = view.findViewById(R.id.pergunta);
            count = view.findViewById(R.id.count);
            thumbnail = view.findViewById(R.id.thumbnail);
            overflow = view.findViewById(R.id.overflow);
            close = view.findViewById(R.id.btn_close_cadeira);
        }
    }

    public List<Cadeira> getCadeiraList() {
        return cadeiraList;
    }
}
