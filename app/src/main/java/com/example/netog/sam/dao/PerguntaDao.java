/**
 * @author: neto
 * @e-mail: netogdn999@gmail.com
 * @license:
 *
 **/
package com.example.netog.sam.dao;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.example.netog.sam.model.Pergunta;
import java.util.ArrayList;
import java.util.List;

public class PerguntaDao extends Dao<Pergunta> {
    private SQLiteDatabase bdw;
    private SQLiteDatabase bdr;

    public PerguntaDao(Context context) {
        super(BDCore.getInstance(context));
    }

    @Override
    public void cadastrar(Pergunta objeto) throws Exception {
        try {
            bdw = bdCore.getWritableDatabase();
            ContentValues valores = new ContentValues();
            valores.put("descricao", objeto.getDescricao());
            valores.put("status", objeto.getStatus());
            valores.put("qtdPerguntas", objeto.getQtdRespostas());
            bdw.insert("pergunta", null, valores);
        }catch (Exception e){
            throw new Exception("Erro no cadastrar-pergunta"+e.getMessage());
        }finally {
            bdw.close();
        }
    }

    @Override
    public void remover(Pergunta objeto) throws Exception {
        try {
            bdw = bdCore.getWritableDatabase();
            bdw.delete("pergunta", "_id = ?", new String[]{"" + objeto.getId()});
        }catch (Exception e){
            throw new Exception("Erro no remover-pergunta"+e.getMessage());
        }finally {
            bdw.close();
        }
    }

    @Override
    public void atualizar(Pergunta objeto) throws Exception {
        try {
            bdw = bdCore.getWritableDatabase();
            ContentValues valores = new ContentValues();
            valores.put("descricao", objeto.getDescricao());
            valores.put("status", objeto.getStatus());
            valores.put("qtdPerguntas", objeto.getQtdRespostas());
            bdw.update("pergunta", valores, "_id = ?", new String[]{""+objeto.getId()});
        }catch (Exception e){
            throw new Exception("Erro no atualizar-pergunta"+e.getMessage());
        }finally {
            bdw.close();
        }
    }

    @Override
    public List<Pergunta> listar() {
        List<Pergunta> lista = new ArrayList<>();
        try{
            bdr = bdCore.getReadableDatabase();
            String[] colunas = new String[]{"_id", "descricao", "status", "qtdRespostas"};
            @SuppressLint("Recycle") Cursor cursor = bdr.query("pergunta", colunas, null, null, null, null, "nome ASC");
            if(cursor.getCount() > 0){
                cursor.moveToFirst();
                do{
                    lista.add(new Pergunta(cursor.getInt(0), cursor.getString(1), cursor.getInt(3) != 0, cursor.getInt(4)));
                }while (cursor.moveToNext());
            }
        }catch (Exception e){
            throw new Exception("Erro no listar-pergunta"+e.getMessage());
        }finally {
            bdr.close();
            return lista;
        }
    }

    @Override
    public List<Pergunta> listar(String coluna, String valor) {
        List<Pergunta> lista = new ArrayList<>();
        try{
            bdr = bdCore.getReadableDatabase();
            String[] filtro = new String[]{coluna, valor};
            String[] colunas = new String[]{"_id", "descricao", "status", "qtdRespostas"};
            @SuppressLint("Recycle") Cursor cursor = bdr.query("pergunta", colunas, " ? like '%?%' ", filtro, null, null, "nome ASC");
            if(cursor.getCount() > 0){
                cursor.moveToFirst();
                do{
                    lista.add(new Pergunta(cursor.getInt(0), cursor.getString(1), cursor.getInt(3) != 0, cursor.getInt(4)));
                }while (cursor.moveToNext());
            }
        }catch (Exception e){
            throw new Exception("Erro no listarFiltro-pergunta"+e.getMessage());
        }finally {
            bdr.close();
            return lista;
        }
    }
}
