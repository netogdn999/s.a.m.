/**
 * @author: neto
 * @e-mail: netogdn999@gmail.com
 * @license:
 *
 **/
package com.example.netog.sam.model;

import com.example.netog.sam.controller.ExceptionController;

import org.json.JSONException;
import org.json.JSONObject;

public class Notificacao {
    private String title;
    private String data;
    private String horario;
    private String info;

    public Notificacao(String title, String data, String horario, String info) {
        this.title = title;
        this.data = data;
        this.horario = horario;
        this.info = info;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    @Override
    public String toString(){
        JSONObject json = new JSONObject();
        try {
            json.put("titulo",getTitle());
            json.put("data",getData());
            json.put("horario",getHorario());
            json.put("info",getInfo());
        } catch (JSONException e) {
            ExceptionController.setMensagem(new String[]{"Error","Error Notificação model "+e.getMessage()});
        }
        return json.toString();
    }

    @Override
    public boolean equals(Object obj) {
        return obj.toString().equals(toString());
    }
}
