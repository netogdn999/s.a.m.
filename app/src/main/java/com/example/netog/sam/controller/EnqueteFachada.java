/**
 * @author: neto
 * @e-mail: netogdn999@gmail.com
 * @license:
 *
 **/
package com.example.netog.sam.controller;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;

import com.example.netog.sam.R;
import com.example.netog.sam.model.Enquete;
import com.example.netog.sam.view.CadeiraInfo;
import com.example.netog.sam.view.EnqueteCadeira;

import java.util.List;

public class EnqueteFachada extends Fachada {
    @SuppressLint("StaticFieldLeak")private static EnqueteFachada fachada;

    private EnqueteFachada(Context context) {
        super(context);
        getDao("enquete", context);
    }

    public static EnqueteFachada getInstance(){
        if(EnqueteFachada.fachada == null){
            EnqueteFachada.fachada = new EnqueteFachada(context);
        }
        return EnqueteFachada.fachada;
    }

    public View.OnClickListener abrirInfo(final FragmentTransaction ft){
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fra = new EnqueteCadeira();
                ft.replace(R.id.painel_principal, fra);
                ft.commit();
            }
        };
    }

    public void cadastrar() throws Exception {
        dao.cadastrar(new Enquete());
    }
    public void remover() throws Exception {
        dao.remover(new Enquete());
    }
    public List<Enquete> listar(){
        return dao.listar();
    }
    public List<Enquete> listar(String coluna, String valor){
        return dao.listar(coluna, valor);
    }
}
