/**
 * @author: neto
 * @e-mail: netogdn999@gmail.com
 * @license:
 *
 **/
package com.example.netog.sam.controller;

import android.annotation.SuppressLint;
import android.content.Context;
import com.example.netog.sam.model.Usuario;
import java.util.List;

public class UsuarioFachada extends Fachada {
    @SuppressLint("StaticFieldLeak")private static UsuarioFachada fachada;

    private UsuarioFachada(Context context) {
        super(context);
        getDao("usuario", context);
    }
    public static UsuarioFachada getInstance(){
        if(UsuarioFachada.fachada == null){
            UsuarioFachada.fachada = new UsuarioFachada(context);
        }
        return UsuarioFachada.fachada;
    }

    public void cadastrar() throws Exception {
        dao.cadastrar(new Usuario());
    }
    public void remover() throws Exception {
        dao.remover(new Usuario());
    }
    public void alterar() throws Exception {dao.atualizar(new Usuario());}
    public List<Usuario> listar(){
        return dao.listar();
    }
    public List<Usuario> listar(String coluna, String valor){
        return dao.listar(coluna, valor);
    }
}
