/**
 * @author: neto
 * @e-mail: netogdn999@gmail.com
 * @license:
 *
 **/
package com.example.netog.sam.model;

import com.example.netog.sam.controller.ExceptionController;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class Usuario {
    private int _id;
    private String nome;
    private String login;
    private String senha;
    private List<Agenda> agenda;
    private List<Cadeira> cadeiras;

    public Usuario() {
        super();
    }

    public Usuario(String nome, String login, String senha) {
        this();
        this.nome = nome;
        this.login = login;
        this.senha = senha;
    }

    public Usuario(int _id, String nome, String login, String senha) {
        this(nome, login, senha);
        this._id = _id;
    }
    /*getteres and setteres*/

    public int getId() {
        return _id;
    }

    public void setId(int _id) {
        this._id = _id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public List<Agenda> getAgenda() {
        return agenda;
    }

    public void setAgenda(List<Agenda> agenda) {
        this.agenda = agenda;
    }

    public List<Cadeira> getCadeiras() {
        return cadeiras;
    }

    public void setCadeiras(List<Cadeira> cadeiras) {
        this.cadeiras = cadeiras;
    }

    @Override
    public String toString(){
        JSONObject json = new JSONObject();
        JSONObject jsonAgenda = new JSONObject();
        JSONObject jsonCadeira = new JSONObject();
        try {
            json.put("id",getId());
            json.put("nome",getNome());
            json.put("login",getLogin());
            json.put("senha",getSenha());
            for(Agenda agen: agenda){
                jsonAgenda.put("id",agen.get_id());
                jsonAgenda.put("status",agen.getStatus());
                jsonAgenda.put("descricao",agen.getDescricao());
                jsonAgenda.put("dia",agen.getDia());
            }for(Cadeira cad: cadeiras){
                jsonCadeira.put("id",cad.getId());
                jsonCadeira.put("nome",cad.getNome());
                jsonCadeira.put("curso",cad.getCurso());
                jsonCadeira.put("semestre",cad.getSemestre());
            }
            json.put("agenda",jsonAgenda);
            json.put("cadeiras",jsonCadeira);
        } catch (JSONException e) {
            ExceptionController.setMensagem(new String[]{"Error","Error Usuario model "+e.getMessage()});
        }
        return json.toString();
    }

    @Override
    public boolean equals(Object obj) {
        return obj.toString().equals(toString());
    }
}
