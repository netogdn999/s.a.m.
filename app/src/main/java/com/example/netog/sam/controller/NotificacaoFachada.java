/**
 * @author: neto
 * @e-mail: netogdn999@gmail.com
 * @license:
 *
 **/
package com.example.netog.sam.controller;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;

import com.example.netog.sam.R;
import com.example.netog.sam.model.Enquete;
import com.example.netog.sam.view.CadeiraInfo;
import com.example.netog.sam.view.NotificacoesCadeira;

import java.io.IOException;
import java.util.List;

public class NotificacaoFachada extends Fachada {
    @SuppressLint("StaticFieldLeak")private static NotificacaoFachada fachada;

    private NotificacaoFachada(Context context) throws IOException {
        super(context);
        getDao("notificacao", context);
        http("https://api.github.com/23");
    }

    public static NotificacaoFachada getInstance(){
        if(NotificacaoFachada.fachada == null){
            try {
                NotificacaoFachada.fachada = new NotificacaoFachada(context);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return NotificacaoFachada.fachada;
    }

    public View.OnClickListener abrirInfo(final FragmentTransaction ft){
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fra = new NotificacoesCadeira();
                ft.replace(R.id.painel_principal, fra);
                ft.commit();
            }
        };
    }

    public void cadastrar(){
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    dao.cadastrar(null);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
    public void remover() throws Exception {

    }
    public List<Enquete> listar(){
        return null;
    }
    public List<Enquete> listar(String coluna, String valor){
        return null;
    }
}
