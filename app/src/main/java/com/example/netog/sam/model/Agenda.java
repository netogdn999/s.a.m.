/**
 * @author: neto
 * @e-mail: netogdn999@gmail.com
 * @license:
 *
 **/
package com.example.netog.sam.model;

import com.example.netog.sam.controller.ExceptionController;

import org.json.JSONException;
import org.json.JSONObject;

public class Agenda {
    private int _id;
    private int status;
    private String descricao;
    private String dia;
    private String horario;

    //toDo:criar metodos equals e o toString, definir licença
    public Agenda() {
        super();
    }

    public Agenda(int status, String descricao, String dia, String horario) {
        this();
        this.status = status;
        this.descricao = descricao;
        this.dia = dia;
        this.horario = horario;
    }

    public Agenda(int _id, int status, String descricao, String dia, String horario) {
        this(status, descricao, dia, horario);
        this._id = _id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getDia() {
        return dia;
    }

    public void setDia(String dia) {
        this.dia = dia;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    @Override
    public String toString(){
        JSONObject json = new JSONObject();
        try {
            json.put("id",get_id());
            json.put("status",getStatus());
            json.put("descricao",getDescricao());
            json.put("dia",getDia());
            json.put("horario",getHorario());
        } catch (JSONException e) {
            ExceptionController.setMensagem(new String[]{"Error","Error Agenda model "+e.getMessage()});
        }
        return json.toString();
    }

    @Override
    public boolean equals(Object obj) {
        return obj.toString().equals(toString());
    }
}
