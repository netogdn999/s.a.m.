/**
 * @author: neto
 * @e-mail: netogdn999@gmail.com
 * @license:
 *
 **/
package com.example.netog.sam.controller;

import android.annotation.SuppressLint;
import android.content.Context;
import com.example.netog.sam.model.Pergunta;
import java.util.List;

public class PerguntaFachada extends Fachada {
    @SuppressLint("StaticFieldLeak")private static PerguntaFachada fachada;

    private PerguntaFachada(Context context) {
        super(context);
        getDao("pergunta", context);
    }

    public static PerguntaFachada getInstance(){
        if(PerguntaFachada.fachada == null){
            PerguntaFachada.fachada = new PerguntaFachada(context);
        }
        return PerguntaFachada.fachada;
    }

    public void cadastrar() throws Exception {
        dao.cadastrar(new Pergunta());
    }
    public void remover() throws Exception {
        dao.remover(new Pergunta());
    }
    public List<Pergunta> listar(){
        return dao.listar();
    }
    public List<Pergunta> listar(String coluna, String valor){
        return dao.listar(coluna, valor);
    }
}
