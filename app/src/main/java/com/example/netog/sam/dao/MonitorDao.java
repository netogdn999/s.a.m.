/**
 * @author: neto
 * @e-mail: netogdn999@gmail.com
 * @license:
 *
 **/
package com.example.netog.sam.dao;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.example.netog.sam.model.Monitor;
import java.util.ArrayList;
import java.util.List;

public class MonitorDao extends Dao<Monitor> {
    private SQLiteDatabase bdw;
    private SQLiteDatabase bdr;

    public MonitorDao(Context context) {
        super(BDCore.getInstance(context));
    }

    @Override
    public void cadastrar(Monitor objeto) throws Exception {
        try {
            bdw = bdCore.getWritableDatabase();
            ContentValues valores = new ContentValues();
            valores.put("idUsuario", objeto.getId());
            bdw.insert("monitor", null, valores);
        }catch (Exception e){
            throw new Exception("Erro no cadastrar-monitor"+e.getMessage());
        }finally {
            bdw.close();
        }
    }

    @Override
    public void remover(Monitor objeto) throws Exception {
        try {
            bdw = bdCore.getWritableDatabase();
            bdw.delete("monitor", "_id = ?", new String[]{"" + objeto.getIdMonitor()});
        }catch (Exception e){
            throw new Exception("Erro no remover-monitor"+e.getMessage());
        }finally {
            bdw.close();
        }
    }

    @Override
    public void atualizar(Monitor objeto) throws Exception {
        try {
            bdw = bdCore.getWritableDatabase();
            ContentValues valores = new ContentValues();
            valores.put("idUsuario", objeto.getId());
            bdw.update("monitor", valores, "_id = ?", new String[]{""+objeto.getIdMonitor()});
        }catch (Exception e){
            throw new Exception("Erro no atualizar-monitor"+e.getMessage());
        }finally {
            bdw.close();
        }
    }

    @Override
    public List<Monitor> listar() {
        List<Monitor> lista = new ArrayList<>();
        try{
            bdr = bdCore.getReadableDatabase();
            @SuppressLint("Recycle") Cursor cursor = bdr.rawQuery("select monitor._id,usuario._id as idUsuario, nome, login, senha from monitor join usuario on monitor.idUsuario = usuario._id;", null);
            if(cursor.getCount() > 0){
                cursor.moveToFirst();
                do{
                    lista.add(new Monitor(cursor.getInt(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getInt(0)));
                }while (cursor.moveToNext());
            }
        }catch (Exception e){
            throw new Exception("Erro no listar-monitor"+e.getMessage());
        }finally {
            bdr.close();
            return lista;
        }
    }

    @Override
    public List<Monitor> listar(String coluna, String valor) {
        List<Monitor> lista = new ArrayList<>();
        try{
            bdr = bdCore.getReadableDatabase();
            String[] filtro = new String[]{coluna, valor};
            @SuppressLint("Recycle") Cursor cursor = bdr.rawQuery("select monitor._id,usuario._id as idUsuario, nome, login, senha from monitor join usuario on monitor.idUsuario = usuario._id where ? like '%?%';", filtro);
            if(cursor.getCount() > 0){
                cursor.moveToFirst();
                do{
                    lista.add(new Monitor(cursor.getInt(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getInt(0)));
                }while (cursor.moveToNext());
            }
        }catch (Exception e){
            throw new Exception("Erro no listarFiltro-monitor"+e.getMessage());
        }finally {
            bdr.close();
            return lista;
        }
    }
}
