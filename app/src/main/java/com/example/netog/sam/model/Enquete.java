/**
 * @author: neto
 * @e-mail: netogdn999@gmail.com
 * @license:
 *
 **/
package com.example.netog.sam.model;

import com.example.netog.sam.controller.ExceptionController;

import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

public class Enquete {
    private int _id;
    private int idUsuario;
    private List<Pergunta> perguntas;
    private Date dataInicial;
    private Date dataFinal;

    public Enquete() {
        super();
    }

    public Enquete(int idUsuario, Date dataInicial, Date dataFinal) {
        this();
        this.idUsuario = idUsuario;
        this.dataInicial = dataInicial;
        this.dataFinal = dataFinal;
    }

    public Enquete(int _id, int idUsuario, Date dataInicial, Date dataFinal) {
        this(idUsuario, dataInicial, dataFinal);
        this._id = _id;
    }

    public Enquete(Pergunta pergunta){
        super();
        if(perguntas == null){
            perguntas = new ArrayList<>();
        }
        perguntas.add(pergunta);
    }

    public int getId() {
        return _id;
    }

    public void setId(int _id) {
        this._id = _id;
    }

    public List<Pergunta> getPerguntas() {
        return perguntas;
    }

    public void setPerguntas(List<Pergunta> perguntas) {
        this.perguntas = perguntas;
    }

    public Date getDataInicial() {
        return dataInicial;
    }

    public void setDataInicial(Date dataInicial) {
        this.dataInicial = dataInicial;
    }

    public Date getDataFinal() {
        return dataFinal;
    }

    public void setDataFinal(Date dataFinal) {
        this.dataFinal = dataFinal;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    @Override
    public String toString(){
        JSONObject json = new JSONObject();
        JSONObject jsonPerguntas = new JSONObject();
        try {
            json.put("id",getId());
            json.put("idUsuario",getIdUsuario());
            for(Pergunta perg: perguntas){
                jsonPerguntas.put("id",perg.getId());
                jsonPerguntas.put("descricao",perg.getDescricao());
                jsonPerguntas.put("status",perg.getStatus());
                jsonPerguntas.put("qtdRespostas",perg.getQtdRespostas());
            }
            json.put("perguntas",jsonPerguntas);
            json.put("dataInicial",getDataInicial());
            json.put("dataFinal",getDataFinal());
        } catch (JSONException e) {
            ExceptionController.setMensagem(new String[]{"Error","Error Enquete model "+e.getMessage()});
        }
        return json.toString();
    }

    @Override
    public boolean equals(Object obj) {
        return obj.toString().equals(toString());
    }
}
