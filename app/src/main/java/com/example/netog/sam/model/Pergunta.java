/**
 * @author: neto
 * @e-mail: netogdn999@gmail.com
 * @license:
 *
 **/
package com.example.netog.sam.model;

import com.example.netog.sam.controller.ExceptionController;

import org.json.JSONException;
import org.json.JSONObject;

public class Pergunta {
    private int _id;
    private String descricao;
    private Boolean status;
    private int qtdRespostas;

    public Pergunta() {
        super();
    }

    public Pergunta(String descricao, Boolean status, int qtdRespostas) {
        this();
        this.descricao = descricao;
        this.status = status;
        this.qtdRespostas = qtdRespostas;
    }

    public Pergunta(int _id, String descricao, Boolean status, int qtdRespostas) {
        this(descricao, status, qtdRespostas);
        this._id = _id;
    }

    public Pergunta(String descricao){
        super();
        this.descricao = descricao;
    }

    public int getId() {
        return _id;
    }

    public void setId(int _id) {
        this._id = _id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public int getQtdRespostas() {
        return qtdRespostas;
    }

    public void setQtdRespostas(int qtdRespostas) {
        this.qtdRespostas = qtdRespostas;
    }

    @Override
    public String toString(){
        JSONObject json = new JSONObject();
        try {
            json.put("id",getId());
            json.put("descricao",getDescricao());
            json.put("status",getStatus());
            json.put("qtdRespostas",getQtdRespostas());
        } catch (JSONException e) {
            ExceptionController.setMensagem(new String[]{"Error","Error Pergunta model "+e.getMessage()});
        }
        return json.toString();
    }

    @Override
    public boolean equals(Object obj) {
        return obj.toString().equals(toString());
    }
}
