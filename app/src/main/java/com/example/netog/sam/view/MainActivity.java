package com.example.netog.sam.view;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.example.netog.sam.R;

@SuppressLint("Registered")
public class MainActivity extends AppCompatActivity {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        Button btnLogar = findViewById(R.id.logar);
        Log.i("Login","Entrou");
        btnLogar.setOnClickListener(logar());
    }

    private View.OnClickListener logar(){
        return new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getBaseContext(), TelaPrincipal.class));
            }
        };
    }
}
