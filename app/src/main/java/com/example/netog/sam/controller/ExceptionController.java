/**
 * @author: neto
 * @e-mail: netogdn999@gmail.com
 * @license:
 *
 **/
package com.example.netog.sam.controller;

import java.util.ArrayList;
import java.util.List;

public class ExceptionController {

    private static final List<String[]> mensagem = new ArrayList<>();

    public static List<String[]> getMensagem() {
        List<String[]> aux = mensagem;
        mensagem.clear();
        return aux;
    }

    public static void setMensagem(String[] mensagem) {
        ExceptionController.mensagem.add(mensagem);
    }
}
