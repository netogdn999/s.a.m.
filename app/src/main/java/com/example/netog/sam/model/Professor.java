/**
 * @author: neto
 * @e-mail: netogdn999@gmail.com
 * @license:
 *
 **/
package com.example.netog.sam.model;

import com.example.netog.sam.controller.ExceptionController;

import org.json.JSONException;
import org.json.JSONObject;

public class Professor extends Usuario {
    private int idProfessor;

    public Professor() {
        super();
    }

    public Professor(int idUsuario, String nome, String login, String senha, int idProfessor) {
        super(idUsuario, nome, login, senha);
        this.idProfessor = idProfessor;
    }

    public int getIdProfessor() {
        return idProfessor;
    }

    public void setIdProfessor(int idProfessor) {
        this.idProfessor = idProfessor;
    }

    @Override
    public String toString(){
        JSONObject json = new JSONObject();
        JSONObject jsonAgenda = new JSONObject();
        JSONObject jsonCadeira = new JSONObject();
        try {
            json.put("id",getIdProfessor());
            json.put("idUsuario",getId());
            json.put("nome",getNome());
            json.put("login",getLogin());
            json.put("senha",getSenha());
            for(Agenda agen: getAgenda()){
                jsonAgenda.put("id",agen.get_id());
                jsonAgenda.put("status",agen.getStatus());
                jsonAgenda.put("descricao",agen.getDescricao());
                jsonAgenda.put("dia",agen.getDia());
            }for(Cadeira cad: getCadeiras()){
                jsonCadeira.put("id",cad.getId());
                jsonCadeira.put("nome",cad.getNome());
                jsonCadeira.put("curso",cad.getCurso());
                jsonCadeira.put("semestre",cad.getSemestre());
            }
            json.put("agenda",jsonAgenda);
            json.put("cadeiras",jsonCadeira);
        } catch (JSONException e) {
            ExceptionController.setMensagem(new String[]{"Error","Error Professor model "+e.getMessage()});
        }
        return json.toString();
    }

    @Override
    public boolean equals(Object obj) {
        return obj.toString().equals(toString());
    }
}
