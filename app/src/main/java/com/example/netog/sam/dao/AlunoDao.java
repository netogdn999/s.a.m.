/**
 * @author: neto
 * @e-mail: netogdn999@gmail.com
 * @license:
 *
 **/
package com.example.netog.sam.dao;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.example.netog.sam.model.Aluno;
import java.util.ArrayList;
import java.util.List;

public class AlunoDao extends Dao<Aluno> {
    private SQLiteDatabase bdw;
    private SQLiteDatabase bdr;

    public AlunoDao(Context context) {
        super(BDCore.getInstance(context));
    }

    @Override
    public void cadastrar(Aluno objeto) throws Exception {
        try {
            bdw = bdCore.getWritableDatabase();
            ContentValues valores = new ContentValues();
            valores.put("idUsuario", objeto.getId());
            valores.put("curso", objeto.getCurso());
            valores.put("semestre", objeto.getSemestre());
            bdw.insert("aluno", null, valores);
        }catch (Exception e){
            throw new Exception("Erro no cadastrar-aluno"+e.getMessage());
        }finally {
            bdw.close();
        }
    }

    @Override
    public void remover(Aluno objeto) throws Exception {
        try {
            bdw = bdCore.getWritableDatabase();
            bdw.delete("aluno", "_id = ?", new String[]{"" + objeto.getIdAluno()});
        }catch (Exception e){
            throw new Exception("Erro no remover-aluno"+e.getMessage());
        }finally {
            bdw.close();
        }
    }

    @Override
    public void atualizar(Aluno objeto) throws Exception {
        try {
            bdw = bdCore.getWritableDatabase();
            ContentValues valores = new ContentValues();
            valores.put("idUsuario", objeto.getId());
            valores.put("curso", objeto.getCurso());
            valores.put("semestre", objeto.getSemestre());
            bdw.update("aluno", valores, "_id = ?", new String[]{""+objeto.getIdAluno()});
        }catch (Exception e){
            throw new Exception("Erro no atualizar-aluno"+e.getMessage());
        }finally {
            bdw.close();
        }
    }

    @Override
    public List<Aluno> listar() {
        List<Aluno> lista = new ArrayList<>();
        try{
            bdr = bdCore.getReadableDatabase();
            String[] colunas = new String[]{"idUsuario", "nome", "login", "senha", "_id"};
            @SuppressLint("Recycle") Cursor cursor = bdr.query("Aluno", colunas, null, null, null, null, "nome ASC");
            if(cursor.getCount() > 0){
                cursor.moveToFirst();
                do{
                    lista.add(new Aluno(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getInt(4)));
                }while (cursor.moveToNext());
            }
        }catch (Exception e){
            throw new Exception("Erro no listar-aluno"+e.getMessage());
        }finally {
            bdr.close();
            return lista;
        }
    }

    @Override
    public List<Aluno> listar(String coluna, String valor) {
        List<Aluno> lista = new ArrayList<>();
        try{
            bdr = bdCore.getReadableDatabase();
            String[] filtro = new String[]{coluna, valor};
            String[] colunas = new String[]{"idUsuario", "nome", "login", "senha", "_id"};
            @SuppressLint("Recycle") Cursor cursor = bdr.query("Aluno", colunas, " ? like '%?%' ", filtro, null, null, "nome ASC");
            if(cursor.getCount() > 0){
                cursor.moveToFirst();
                do{
                    lista.add(new Aluno(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getInt(4)));
                }while (cursor.moveToNext());
            }
        }catch (Exception e){
            throw new Exception("Erro no listarFiltro-aluno"+e.getMessage());
        }finally {
            bdr.close();
            return lista;
        }
    }
}
