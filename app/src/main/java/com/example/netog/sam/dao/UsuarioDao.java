/**
 * @author: neto
 * @e-mail: netogdn999@gmail.com
 * @license:
 *
 **/
package com.example.netog.sam.dao;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.example.netog.sam.model.Agenda;
import com.example.netog.sam.model.Cadeira;
import com.example.netog.sam.model.Usuario;
import java.util.ArrayList;
import java.util.List;

public class UsuarioDao extends Dao<Usuario> {
    private SQLiteDatabase bdw;
    private SQLiteDatabase bdr;

    public UsuarioDao(Context context) {
        super(BDCore.getInstance(context));
    }

    @Override
    public void cadastrar(Usuario objeto) throws Exception {
        try {
            bdw = bdCore.getWritableDatabase();
            ContentValues valores = new ContentValues();
            valores.put("nome", objeto.getNome());
            valores.put("login", objeto.getLogin());
            valores.put("senha", objeto.getSenha());
            bdw.insert("usuario", null, valores);
        }catch (Exception e){
            throw new Exception("Erro no cadastrar-Usuario"+e.getMessage());
        }finally {
            bdw.close();
        }
    }

    @Override
    public void remover(Usuario objeto) throws Exception {
        try {
            bdw = bdCore.getWritableDatabase();
            bdw.delete("usuario", "_id = ?", new String[]{"" + objeto.getId()});
        }catch (Exception e){
            throw new Exception("Erro no remover-Usuario"+e.getMessage());
        }finally {
            bdw.close();
        }
    }

    @Override
    public void atualizar(Usuario objeto) throws Exception {
        try {
            bdw = bdCore.getWritableDatabase();
            ContentValues valores = new ContentValues();
            valores.put("nome", objeto.getNome());
            valores.put("login", objeto.getLogin());
            valores.put("senha", objeto.getSenha());
            bdw.update("usuario", valores, "_id = ?", new String[]{""+objeto.getId()});
        }catch (Exception e){
            throw new Exception("Erro no atualizar-Usuario"+e.getMessage());
        }finally {
            bdw.close();
        }
    }

    @Override
    public List<Usuario> listar() {
        List<Usuario> lista = new ArrayList<>();
        try{
            bdr = bdCore.getReadableDatabase();
            String[] colunas = new String[]{"_id", "nome", "login", "senha"};
            @SuppressLint("Recycle") Cursor cursor = bdr.query("usuario", colunas, null, null, null, null, "nome ASC");
            if(cursor.getCount() > 0){
                cursor.moveToFirst();
                do{
                    lista.add(new Usuario(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getString(3)));
                }while (cursor.moveToNext());
            }
        }catch (Exception e){
            throw new Exception("Erro no listar-Usuario"+e.getMessage());
        }finally {
            bdr.close();
            return lista;
        }
    }

    @Override
    public List<Usuario> listar(String coluna, String valor) {
        List<Usuario> lista = new ArrayList<>();
        try{
            bdr = bdCore.getReadableDatabase();
            String[] filtro = new String[]{coluna, valor};
            String[] colunas = new String[]{"_id", "nome", "login", "senha"};
            @SuppressLint("Recycle") Cursor cursor = bdr.query("usuario", colunas, " ? like '%?%' ", filtro, null, null, "nome ASC");
            if(cursor.getCount() > 0){
                cursor.moveToFirst();
                do{
                    lista.add(new Usuario(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getString(3)));
                }while (cursor.moveToNext());
            }
        }catch (Exception e){
            throw new Exception("Erro no listarFiltro-Usuario"+e.getMessage());
        }finally {
            bdr.close();
            return lista;
        }
    }

    public List<Agenda> listarAgenda() {
        List<Agenda> lista = new ArrayList<>();
        try{
            bdr = bdCore.getReadableDatabase();
            @SuppressLint("Recycle") Cursor cursor = bdr.rawQuery("select a._id,status, descricao, dia, horario from agenda as a join usuario_agenda as ua on ua.idAgenda = a._id join usuario as u on u._id=ua.idUsuario;", null);
            if(cursor.getCount() > 0){
                cursor.moveToFirst();
                do{
                    lista.add(new Agenda(cursor.getInt(0), cursor.getInt(1), cursor.getString(2), cursor.getString(3), cursor.getString(4)));
                }while (cursor.moveToNext());
            }
        }catch (Exception e){
            throw new Exception("Erro no listarAgenda-Usuario"+e.getMessage());
        }finally {
            bdr.close();
            return lista;
        }
    }

    public List<Cadeira> listarCadeira() {
        List<Cadeira> lista = new ArrayList<>();
        try{
            bdr = bdCore.getReadableDatabase();
            @SuppressLint("Recycle") Cursor cursor = bdr.rawQuery("select c._id, c.nome, curso, semestre from cadeira as c join usuario_cadeira as uc on uc.idCadeira = c._id join usuario as u on u._id=uc.idUsuario;", null);
            if(cursor.getCount() > 0){
                cursor.moveToFirst();
                do{
                    lista.add(new Cadeira(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getInt(3)));
                }while (cursor.moveToNext());
            }
        }catch (Exception e){
            throw new Exception("Erro no listarCadeira-Usuario"+e.getMessage());
        }finally {
            bdr.close();
            return lista;
        }
    }
}
