/**
 * @author: neto
 * @e-mail: netogdn999@gmail.com
 * @license:
 *
 **/
package com.example.netog.sam.dao;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.example.netog.sam.model.Enquete;
import com.example.netog.sam.model.Pergunta;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

public class EnqueteDao extends Dao<Enquete> {
    private SQLiteDatabase bdw;
    private SQLiteDatabase bdr;

    public EnqueteDao(Context context) {
        super(BDCore.getInstance(context));
    }

    @Override
    public void cadastrar(Enquete objeto) throws Exception {
        try {
            bdw = bdCore.getWritableDatabase();
            ContentValues valores = new ContentValues();
            valores.put("idUsuario", objeto.getIdUsuario());
            valores.put("dataInicial", objeto.getDataInicial().getTime());
            valores.put("dataFinal", objeto.getDataFinal().getTime());
            bdw.insert("enquete", null, valores);
        }catch (Exception e){
            throw new Exception("Erro no cadastrar-enquete"+e.getMessage());
        }finally {
            bdw.close();
        }
    }

    @Override
    public void remover(Enquete objeto) throws Exception {
        try {
            bdw = bdCore.getWritableDatabase();
            bdw.delete("enquete", "_id = ?", new String[]{"" + objeto.getId()});
        }catch (Exception e){
            throw new Exception("Erro no remover-enquete"+e.getMessage());
        }finally {
            bdw.close();
        }
    }

    @Override
    public void atualizar(Enquete objeto) throws Exception {
        try {
            bdw = bdCore.getWritableDatabase();
            ContentValues valores = new ContentValues();
            valores.put("idUsuario", objeto.getIdUsuario());
            valores.put("dataInicial", objeto.getDataInicial().getTime());
            valores.put("dataFinal", objeto.getDataFinal().getTime());
            bdw.update("enquete", valores, "_id = ?", new String[]{""+objeto.getId()});
        }catch (Exception e){
            throw new Exception("Erro no atualizar-enquete"+e.getMessage());
        }finally {
            bdw.close();
        }
    }

    @Override
    public List<Enquete> listar() {
        List<Enquete> lista = new ArrayList<>();
        try{
            bdr = bdCore.getReadableDatabase();
            String[] colunas = new String[]{"_id", "idUsuario", "dataInicial", "dataFinal"};
            @SuppressLint("Recycle") Cursor cursor = bdr.query("enquete", colunas, null, null, null, null, "nome ASC");
            if(cursor.getCount() > 0){
                cursor.moveToFirst();
                do{
                    lista.add(new Enquete(cursor.getInt(0), cursor.getInt(1), new Date(cursor.getInt(2)), new Date(cursor.getInt(2))));
                }while (cursor.moveToNext());
            }
        }catch (Exception e){
            throw new Exception("Erro no listar-Enquete"+e.getMessage());
        }finally {
            bdr.close();
            return lista;
        }
    }

    @Override
    public List<Enquete> listar(String coluna, String valor) {
        List<Enquete> lista = new ArrayList<>();
        try{
            bdr = bdCore.getReadableDatabase();
            String[] filtro = new String[]{coluna, valor};
            String[] colunas = new String[]{"_id", "idUsuario", "dataInicial", "dataFinal"};
            @SuppressLint("Recycle") Cursor cursor = bdr.query("enquete", colunas, " ? like '%?%' ", filtro, null, null, "nome ASC");
            if(cursor.getCount() > 0){
                cursor.moveToFirst();
                do{
                    lista.add(new Enquete(cursor.getInt(0), cursor.getInt(1), new Date(cursor.getInt(2)), new Date(cursor.getInt(2))));
                }while (cursor.moveToNext());
            }
        }catch (Exception e){
            throw new Exception("Erro no listarFiltro-Enquete"+e.getMessage());
        }finally {
            bdr.close();
            return lista;
        }
    }

    public List<Pergunta> listarPergunta() {
        List<Pergunta> lista = new ArrayList<>();
        try{
            bdr = bdCore.getReadableDatabase();
            @SuppressLint("Recycle") Cursor cursor = bdr.rawQuery("select p._id, descricao, status, qtdRespostas from pergunta as p join pergunta_enquete as pe on pe.idPergunta = p._id join enquete as e on e._id=pe.idEnquete;", null);
            if(cursor.getCount() > 0){
                cursor.moveToFirst();
                do{
                    lista.add(new Pergunta(cursor.getInt(0), cursor.getString(1), cursor.getInt(3) != 0, cursor.getInt(4)));
                }while (cursor.moveToNext());
            }
        }catch (Exception e){
            throw new Exception("Erro no listarPergunta-Enquete"+e.getMessage());
        }finally {
            bdr.close();
            return lista;
        }
    }
}
