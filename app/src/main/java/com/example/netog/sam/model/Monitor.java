/**
 * @author: neto
 * @e-mail: netogdn999@gmail.com
 * @license:
 *
 **/
package com.example.netog.sam.model;

import com.example.netog.sam.controller.ExceptionController;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class Monitor extends Aluno {
    private int idMonitor;
    private List<Cadeira> cadeirasMonitora;

    public Monitor() {
        super();
    }

    public Monitor(int idUsuario, String nome, String login, String senha, int idMonitor) {
        super(idUsuario, nome, login, senha);
        this.idMonitor = idMonitor;
    }

    public List<Cadeira> getCadeirasMonitora() {
        return cadeirasMonitora;
    }

    public void setCadeirasMonitora(List<Cadeira> cadeirasMonitora) {
        this.cadeirasMonitora = cadeirasMonitora;
    }

    public int getIdMonitor() {
        return idMonitor;
    }

    public void setIdMonitor(int idMonitor) {
        this.idMonitor = idMonitor;
    }

    @Override
    public String toString(){
        JSONObject json = new JSONObject();
        JSONObject jsonAgenda = new JSONObject();
        JSONObject jsonCadeira = new JSONObject();
        JSONObject jsonCadeirasMonitoria = new JSONObject();
        try {
            json.put("id",getIdMonitor());
            json.put("idUsuario",getId());
            json.put("nome",getNome());
            json.put("login",getLogin());
            json.put("senha",getSenha());
            for(Cadeira cad: cadeirasMonitora){
                jsonCadeirasMonitoria.put("id",cad.getId());
                jsonCadeirasMonitoria.put("nome",cad.getNome());
                jsonCadeirasMonitoria.put("curso",cad.getCurso());
                jsonCadeirasMonitoria.put("semestre",cad.getSemestre());
            }for(Agenda agen: getAgenda()){
                jsonAgenda.put("id",agen.get_id());
                jsonAgenda.put("status",agen.getStatus());
                jsonAgenda.put("descricao",agen.getDescricao());
                jsonAgenda.put("dia",agen.getDia());
            }for(Cadeira cad: getCadeiras()){
                jsonCadeira.put("id",cad.getId());
                jsonCadeira.put("nome",cad.getNome());
                jsonCadeira.put("curso",cad.getCurso());
                jsonCadeira.put("semestre",cad.getSemestre());
            }
            json.put("cadeirasMonitoria",jsonCadeirasMonitoria);
            json.put("agenda",jsonAgenda);
            json.put("cadeiras",jsonCadeira);
        } catch (JSONException e) {
            ExceptionController.setMensagem(new String[]{"Error","Error Monitor model "+e.getMessage()});
        }
        return json.toString();
    }

    @Override
    public boolean equals(Object obj) {
        return obj.toString().equals(toString());
    }
}
