/**
 * @author: neto
 * @e-mail: netogdn999@gmail.com
 * @license:
 *
 **/
package com.example.netog.sam.dao;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.example.netog.sam.model.Cadeira;
import java.util.ArrayList;
import java.util.List;

public class CadeiraDao extends Dao<Cadeira> {
    private SQLiteDatabase bdw;
    private SQLiteDatabase bdr;

    public CadeiraDao(Context context) {
        super(BDCore.getInstance(context));
    }

    @Override
    public void cadastrar(Cadeira objeto) throws Exception {
        try {
            bdw = bdCore.getWritableDatabase();
            ContentValues valores = new ContentValues();
            valores.put("nome", objeto.getNome());
            valores.put("curso", objeto.getCurso());
            valores.put("semestre", objeto.getSemestre());
            bdw.insert("cadeira", null, valores);
        }catch (Exception e){
            throw new Exception("Erro no cadastrar-cadeira"+e.getMessage());
        }finally {
            bdw.close();
        }
    }

    @Override
    public void remover(Cadeira objeto) throws Exception {
        try {
            bdw = bdCore.getWritableDatabase();
            bdw.delete("cadeira", "_id = ?", new String[]{"" + objeto.getId()});
        }catch (Exception e){
            throw new Exception("Erro no remover-cadeira"+e.getMessage());
        }finally {
            bdw.close();
        }
    }

    @Override
    public void atualizar(Cadeira objeto) throws Exception {
        try {
            bdw = bdCore.getWritableDatabase();
            ContentValues valores = new ContentValues();
            valores.put("nome", objeto.getNome());
            valores.put("curso", objeto.getCurso());
            valores.put("semestre", objeto.getSemestre());
            bdw.update("cadeira", valores, "_id = ?", new String[]{""+objeto.getId()});
        }catch (Exception e){
            throw new Exception("Erro no atualizar-cadeira"+e.getMessage());
        }finally {
            bdw.close();
        }
    }

    @Override
    public List<Cadeira> listar() {
        List<Cadeira> lista = new ArrayList<>();
        try{
            bdr = bdCore.getReadableDatabase();
            String[] colunas = new String[]{"_id", "nome", "curso", "semestre"};
            @SuppressLint("Recycle") Cursor cursor = bdr.query("cadeira", colunas, null, null, null, null, "nome ASC");
            if(cursor.getCount() > 0){
                cursor.moveToFirst();
                do{
                    lista.add(new Cadeira(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getInt(3)));
                }while (cursor.moveToNext());
            }
        }catch (Exception e){
            throw new Exception("Erro no listar-cadeira"+e.getMessage());
        }finally {
            bdr.close();
            return lista;
        }
    }

    @Override
    public List<Cadeira> listar(String coluna, String valor) {
        List<Cadeira> lista = new ArrayList<>();
        try{
            bdr = bdCore.getReadableDatabase();
            String[] filtro = new String[]{coluna, valor};
            String[] colunas = new String[]{"_id", "nome", "curso", "semestre"};
            @SuppressLint("Recycle") Cursor cursor = bdr.query("cadeira", colunas, " ? like '%?%' ", filtro, null, null, "nome ASC");
            if(cursor.getCount() > 0){
                cursor.moveToFirst();
                do{
                    lista.add(new Cadeira(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getInt(3)));
                }while (cursor.moveToNext());
            }
        }catch (Exception e){
            throw new Exception("Erro no listarFiltro-cadeira"+e.getMessage());
        }finally {
            bdr.close();
            return lista;
        }
    }
}
