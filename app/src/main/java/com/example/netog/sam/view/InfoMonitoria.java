package com.example.netog.sam.view;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.netog.sam.R;

import java.util.Objects;

public class InfoMonitoria extends BaseFragment {

    @SuppressLint("SetTextI18n")
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.info_monitoria, container, false);
        TextView horario, sala, sobre;
        horario = view.findViewById(R.id.horario);
        horario.setText("8:00 - 10:00 / 13:30 - 15:30");
        sala = view.findViewById(R.id.sala);
        sala.setText("09");
        sobre = view.findViewById(R.id.sobre);
        sobre.setText("As monitorias do dia serão dedicadas a resolução da lista 1 e para tirar dúvidas sobre o trabalho");
        return view;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onBackPressed() {
        @SuppressLint("CommitTransaction") FragmentTransaction ft = Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.painel_principal, new CadeiraInfo());
        ft.commit();
    }
}
