/**
 * @author: neto
 * @e-mail: netogdn999@gmail.com
 * @license:
 *
 **/
package com.example.netog.sam.controller;

import android.annotation.SuppressLint;
import android.content.Context;
import com.example.netog.sam.model.Professor;
import java.util.List;

public class ProfessorFachada extends Fachada {
    @SuppressLint("StaticFieldLeak")private static ProfessorFachada fachada;

    private ProfessorFachada(Context context) {
        super(context);
        getDao("professor", context);
    }

    public static ProfessorFachada getInstance(){
        if(ProfessorFachada.fachada == null){
            ProfessorFachada.fachada = new ProfessorFachada(context);
        }
        return ProfessorFachada.fachada;
    }

    public void cadastrar() throws Exception {
        dao.cadastrar(new Professor());
    }
    public void remover() throws Exception {
        dao.remover(new Professor());
    }
    public List<Professor> listar(){
        return dao.listar();
    }
    public List<Professor> listar(String coluna, String valor){
        return dao.listar(coluna, valor);
    }
}
