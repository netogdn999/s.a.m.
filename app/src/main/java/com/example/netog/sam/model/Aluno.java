/**
 * @author: neto
 * @e-mail: netogdn999@gmail.com
 * @license:
 *
 **/
package com.example.netog.sam.model;

import com.example.netog.sam.controller.ExceptionController;

import org.json.JSONException;
import org.json.JSONObject;

public class Aluno extends Usuario {
    private int idAluno;
    private String curso;
    private int semestre;

    public Aluno() {
        super();
    }

    public Aluno(int idUsuario, String nome, String login, String senha) {
        super(idUsuario, nome, login, senha);
    }

    public Aluno(int idUsuario, String nome, String login, String senha, int idAluno) {
        this(idUsuario, nome, login, senha);
        this.idAluno = idAluno;
    }

    public String getCurso() {
        return curso;
    }

    public void setCurso(String curso) {
        this.curso = curso;
    }

    public int getSemestre() {
        return semestre;
    }

    public void setSemestre(int semestre) {
        this.semestre = semestre;
    }

    public int getIdAluno() {
        return idAluno;
    }

    public void setIdAluno(int idAluno) {
        this.idAluno = idAluno;
    }

    @Override
    public String toString(){
        JSONObject json = new JSONObject();
        JSONObject jsonAgenda = new JSONObject();
        JSONObject jsonCadeira = new JSONObject();
        try {
            json.put("id",getIdAluno());
            json.put("idUsuario",getId());
            json.put("nome",getNome());
            json.put("login",getLogin());
            json.put("senha",getSenha());
            json.put("curso",getCurso());
            json.put("semestre",getSemestre());
            for(Agenda agen: getAgenda()){
                jsonAgenda.put("id",agen.get_id());
                jsonAgenda.put("status",agen.getStatus());
                jsonAgenda.put("descricao",agen.getDescricao());
                jsonAgenda.put("dia",agen.getDia());
            }for(Cadeira cad: getCadeiras()){
                jsonCadeira.put("id",cad.getId());
                jsonCadeira.put("nome",cad.getNome());
                jsonCadeira.put("curso",cad.getCurso());
                jsonCadeira.put("semestre",cad.getSemestre());
            }
            json.put("agenda",jsonAgenda);
            json.put("cadeiras",jsonCadeira);
        } catch (JSONException e) {
            ExceptionController.setMensagem(new String[]{"Error","Error Aluno model "+e.getMessage()});
        }
        return json.toString();
    }

    @Override
    public boolean equals(Object obj) {
        return obj.toString().equals(toString());
    }
}
